# Spring Boot: Integrate Gitlab with Pivotal Cloud Foundry
Deploy Spring Boot application to [Pivotal Cloud Foundry][1] through [Gitlab][2].

## Dependencies:
  - [Spring Boot][7]
  - [Spring WebMvc][5]
  - [Spring Actuator][6]

## Environment:
  - Java 11
  
## Source
Based on [Guide by Gitlab][3].

## Application
Sample application is available at [rashidi-cf-gitlab-impressive-bongo.cfapps.io][4].

[1]: https://github.com/pivotal-cf
[2]: https://gitlab.com/
[3]: https://docs.gitlab.com/ee/ci/examples/deploy_spring_boot_to_cloud_foundry/
[4]: https://rashidi-cf-gitlab-impressive-bongo.cfapps.io/
[5]: https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc
[6]: https://docs.spring.io/spring-boot/docs/current/actuator-api/html/
[7]: https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/