package scratches.cf.gitlab.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Rashidi Zin
 */
@RestController
public class IndexResource {

    @GetMapping("/")
    public String greet() {
        return "Hello there! We're running on Pivotal Cloud Foundry and deployed from Gitlab!";
    }

}
